import os

import webapp2
import jinja2
import models
from bs4 import BeautifulSoup
from google.appengine.ext import db
from google.appengine.api import urlfetch
template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)


def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

class ResultHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        return render_str(template, **params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))
        

class MainPage(ResultHandler):
    def handle_result(self,rpc,regno):
        result = rpc.get_result()
        soup = BeautifulSoup(result.content)
        restext = soup.get_text("|",strip=True)
        exam_res_year = restext.split('UG/PG Results')[1]
        exam_res_year = exam_res_year.split(' ')
        
        exam_held_month = exam_res_year[1]
        year = exam_res_year[4].split("|")[0]
        
        self.write(year)
        
        key_sem = int(year[2:4]) - int(regno[4:6])
        
        if exam_held_month in ('Jan','Feb','Mar','Apr','May','June'):
            sem_type = "even"
        else:
            sem_type = "odd"
        
        if sem_type == "even":
            key_val =  str(key_sem) + "_" + str(2)
        else:
            key_val =  str(key_sem) + "_" + str(1)
                                                
    
        
        
        results = restext.split(regno)[1].split('Click here for Legends')[0]
        #self.write(results + "<br>")
        results = results.split("|")
        results.pop()   #To remove last element from list. It is a blank character
        if len(results) != 0:
            results.pop(0)  #To remove first element from list. It is a blank character
            results.remove('Subject Code') 
            results.remove('Grade')
            results.remove('Result')
        else:
            #self.write("Register number is wrong ! ")
            return None
        self.write(str(results) + "<br><br>")
        #return results
        self.process_result(regno, results,key_val)
    
        
        
    
    
    def create_callback(self,rpc,r):
        return lambda: self.handle_result(rpc,r)
    
    
    def fetch_from_au_site(self,regno):
        url = "http://www.annauniv.edu/cgi-bin/result/cgrade.pl?regno="+regno
        contents = webapp2.urllib.urlopen(url).read()
        soup = BeautifulSoup(contents)
        restext = soup.get_text("|",strip=True)
        exam_res_year = restext.split('UG/PG Results')[1]
        exam_res_year = exam_res_year.split(' ')
        
        exam_held_month = exam_res_year[1]
        year = exam_res_year[4].split("|")[0]
        self.write(year)
        
        key_sem = int(year[2:4]) - int(regno[4:6])
        
        if exam_held_month in ('Jan','Feb','Mar','Apr','May','June'):
            sem_type = "even"
        else:
            sem_type = "odd"
        
        if sem_type == "even":
            key_val =  str(key_sem) + "_" + str(2)
        else:
            key_val =  str(key_sem) + "_" + str(1)
                                                
        
        
        
        
        results = restext.split(regno)[1].split('Click here for Legends')[0]
        #self.write(results + "<br>")
        
        results = results.split("|")
        results.pop()   #To remove last element from list. It is a blank character
        if len(results) != 0:
            results.pop(0)  #To remove first element from list. It is a blank character
            results.remove('Subject Code') 
            results.remove('Grade')
            results.remove('Result')
        else:
            #self.write("Register number is wrong ! ")
            return None
        self.write( str(results) + "<br><br>")
        return results,key_val
    
    def process_result(self,regno,results,key_val):
        # for a particular semester result with no arrears, list length will be atleast 29.
        if len(results) < 29:
            if any('WH' in r for r in results):
                self.write("Withheld Case to be handled")
                subjects = results[2::2]
                self.write(subjects)
                
                # How to put in datastore. Think
                return
            else:
                return
            
            
            
            
            
        studentname = results[0]
        subjects = results[2::3]
        grades = results[3::3]
        self.write("<br> Subjects: " + str(subjects) + "<br>")
        subnames = list()
        skeylist= list()
        for each in subjects:
            skey = db.Key.from_path('Subject',each)
            skeylist.append(skey)
        
        subnames = db.get(skeylist)
            
            #subnames.append(k.subname)
        
        
        if len(regno) == 12:
            coll = regno[0:4]
            yoj = regno[4:6]
            dept = regno[6:9]
            roll = regno[9:]
        
        
        k = db.Key.from_path('CollDept',coll+dept)
        yojkey = db.Key.from_path('YearofJoining',yoj)
        r = models.Result(regno = regno,key_name=regno+"_"+key_val)
        r.colldept = k
        r.name = results[0]
        r.yoj = yojkey
        r.depname = results[1]
        
    
        
        
        
        self.write("<br>Student name:" + studentname)
        for (sub,sname,grade) in zip(subjects,subnames,grades):
            try:
                self.write("<br>" + sub + " :  " + sname.subname + ":" + grade)
                #setattr(r,sub,grade)
                sdkey = db.Key.from_path('SubDept',sub+"_"+dept)
                sd = db.get(sdkey)
                
                setattr(r,sub,sd.sem+'  '+grade+'  '+sd.sub.subname)
                
            except AttributeError:
                pass
            
        
        r.put()
        

    
    def get(self):
        self.render("index.html")
    
    def post(self):
        
        regno = self.request.get('regno')
        reg = regno[0:9]
        rollno = regno[9:]
        urls = []
        regnos = []
        
        fetch_res,key_val = self.fetch_from_au_site(regno)
        
        
        for i in range(1,20):
            if len(str(i)) == 1:
                temp = "00" + str(i)
            elif len(str(i)) == 2:
                temp = "0" + str(i)
            
            regnos.append(reg+temp)
            urls.append("http://www.annauniv.edu/cgi-bin/result/cgrade.pl?regno="+reg+temp)
            
            
        rpcs = []
        for (url,r) in zip(urls,regnos):
            rpc = urlfetch.create_rpc()
            rpc.callback = self.create_callback(rpc,r)
            urlfetch.make_fetch_call(rpc, url)
            rpcs.append(rpc)
        
        self.write(str(fetch_res)+"<br><br>")
        if fetch_res is not None:
            self.process_result(regno,fetch_res,key_val)
        else:
            self.write("No results Available for this number" + "<br> Check your register number again")
                
                
        for rpc in rpcs:
            rpc.wait()
    
        
        
                                    


app = webapp2.WSGIApplication([('/fetch/', MainPage),
                              
                               
                               ],
                              debug=True)
def main():
    app.run()
    

if __name__=='__main__':
    main()