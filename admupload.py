import os
import webapp2
import jinja2
import urllib
import models



from google.appengine.ext import db
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)


def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

class ResultHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        return render_str(template, **params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))


class BaseUploadHandler(blobstore_handlers.BlobstoreUploadHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        return render_str(template, **params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))
        
class BaseDownloadHandler(blobstore_handlers.BlobstoreDownloadHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        return render_str(template, **params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))



class MainHandler(ResultHandler):
    #MainHandler - handler for /admin/files/ url
    #template file used is uploadfile.html
    
    def get(self):
        self.render("uploadfile.html")
        for b in blobstore.BlobInfo.all():
            self.response.out.write('<li><a href="/admin/files/serve/%s' % str(b.key()) + '">' + str(b.filename) + '</a>')
        
        
    def post(self):
        type = self.request.get('type_upload')
        if type == "college":
            self.redirect("/admin/files/college")
        elif type == "subject":
            self.redirect("/admin/files/subject")
        elif type == "dept":
            self.redirect("/admin/files/dept")


class CollegeHandler(BaseUploadHandler):
    # Handler for /admin/files/college url
    # handles upload for college details
    """
    Format:
    Subcode  Subname  Credits  Sem  R/E
    2 spaces are used in between each property
    """
    
    def get(self):
        univ = db.GqlQuery("select * from Univ")
        upload_url = blobstore.create_upload_url("/admin/files/college")
        self.render("collegeupload.html",univ = univ,upload_url=upload_url)
        
        
    
    def post(self):
        
        upload_files = self.get_uploads('upload')
        univ = self.request.get('univ')
        typeofcode = self.request.get('type')
        blob_info = upload_files[0]
      
        blob_key = str(urllib.unquote(str(blob_info.key())))
        if not blobstore.get(blob_key):
            self.error(404)
        else:
            blob_info = blobstore.BlobReader(blob_key)
            #self.write(blob_info.readline(3))
            #self.write(blob_info.readline())
            
            ukey = db.Key.from_path('Univ',univ)
            u = db.get(ukey)
            first_byte = blob_info.readline(3)
            self.response.out.write(first_byte[0:] + "<br>")
            if typeofcode == '2':
                for each in blob_info:
                    line = each.decode('utf-8')
                    line = line.strip()
                    self.response.out.write(line[0:3]+ " " )
                    self.response.out.write(line[5:9] + "   ")
                    self.response.out.write(line[11:])
                    
                    coll = models.College(key_name=line[5:9],colname=line[11:],univcode=u)
                    coll.put()
                    
                    self.response.out.write("--> Done" + "<br>")
            elif typeofcode == '1':
                self.write("Single code ")
                for each in blob_info:
                    line = each.decode('utf-8')
                    line = line.strip()
                    #self.write(line)
                    self.write(line[0:4] + "<br>")
                    self.write(line[6:] +"<br>")
                    #self.response.out.write(line[0:4]+ "  " )
                    #self.response.out.write(line[6:])
                    
                    coll = models.College(key_name=line[0:4],colname=line[6:],univcode=u)
                    coll.put()
                    
                    self.response.out.write("--> Done" + "<br>")
                
                


class SubjectHandler(BaseUploadHandler):
    #Handler for /admin/files/subject url
    #Used to upload subject details, and create SubDept entities too.
    def get(self):
        reguniv = db.GqlQuery("select * from UnivReg")
        dept = db.GqlQuery("select * from Department")
        upload_url = blobstore.create_upload_url("/admin/files/subject")
        self.render("subupload.html",reguniv = reguniv,upload_url=upload_url,dept=dept)
    
    def post(self):
        
        upload_files = self.get_uploads('upload')
        dept = self.request.get('dept')
        dkey = db.Key.from_path('Department',dept)
        d = db.get(dkey)
        blob_info = upload_files[0]
        
        reguniv = self.request.get('reguniv')
        rukey = db.Key.from_path('UnivReg',reguniv)
        ru = db.get(rukey)
        
    
        blob_key = str(urllib.unquote(str(blob_info.key())))
        if not blobstore.get(blob_key):
            self.error(404)
        else:
            blob_info = blobstore.BlobReader(blob_key)
            #first_byte = blob_info.readline(3)
            #self.response.out.write(first_byte[0:] + "<br>")
            
            
            
            for line in blob_info:
                line = line.decode('utf-8')
                line = line.strip()
                l = line.split('  ')
                self.write(l)
                self.write("<br>" + " length: " + str(len(l))+"<br>")
                self.response.out.write( l[0] + "<br>")
                self.response.out.write(l[1] + "<br>" + l[2]+ "<br>")
                self.write(l[3] + "<br>" + l[4]+"<br>")
                
                subj = models.Subject(key_name=l[0],credits=int(l[2]),subname=l[1])
                subj.put()                
                
                kname = l[0]+"_"+dept
                
                sd = models.SubDept(key_name=kname,dept=d,sub=subj,sem=l[3],reguniv=ru)
                if l[4] == "E":
                    sd.elective = "Yes"
                sd.put()
                
                
    
class DeptHandler(BaseUploadHandler):        
    #Handler for /admin/files/dept url
    #Used to upload dept details along with dept code
    
    def get(self):
        upload_url = blobstore.create_upload_url("/admin/files/dept")
        self.render("deptupload.html",upload_url = upload_url)
    
    def post(self):
        upload_files = self.get_uploads('upload')
        blob_info = upload_files[0]
        blob_key = str(urllib.unquote(str(blob_info.key())))
        if not blobstore.get(blob_key):
            self.error(404)
        else:
            blob_info = blobstore.BlobReader(blob_key)
            first_byte = blob_info.readline(3)
            self.response.out.write(first_byte[0:] + "<br>")
            for each in blob_info:
                line = each.decode('utf-8')
                line = line.strip()
                self.response.out.write(line[0:3]+ " " )
                self.response.out.write(line[5:] + "   ")
                #self.response.out.write(line[11:])
                  
                #coll = models.College(key_name=line[5:9],colname=line[11:],univcode=u)
                #coll.put()
                 
                dept = models.Department(key_name=line[0:3],depname=line[5:])
                dept.put()
                 
                self.response.out.write("--> Done" + "<br>")



            
class CollDeptHandler(BaseUploadHandler):        
    #Handler for /admin/files/dept url
    #Used to upload dept details along with dept code
    
    def get(self):
        upload_url = blobstore.create_upload_url("/admin/files/colldept")
        self.render("colldeptupload.html",upload_url = upload_url)
    
    def post(self):
        upload_files = self.get_uploads('upload')
        blob_info = upload_files[0]
        blob_key = str(urllib.unquote(str(blob_info.key())))
        if not blobstore.get(blob_key):
            self.error(404)
        else:
            blob_info = blobstore.BlobReader(blob_key)
            first_byte = blob_info.readline(3)
            self.response.out.write(first_byte[0:] + "<br>")
            for each in blob_info:
                line = each.decode('utf-8')
                line = line.strip()
                line = line.split('  ')
                col = db.Key.from_path('College',line[0])
                for d in line[1::]:
                    dkey = db.Key.from_path('Department',d)
                    
                    c = models.CollDept(key_name=line[0]+d,coll = col,dept = dkey)
                    c.put()
                
                
                
                                 
                self.response.out.write("--> Done" + "<br>")



                
                
        
            
class ServeHandler(BaseDownloadHandler):
    
    def get(self,blob_key):
        
        blob_key = str(urllib.unquote(blob_key))
        if not blobstore.get(blob_key):
            self.error(404)
        else:
            blob_info = blobstore.BlobReader(blob_key)
            self.write(blob_info.readline(3))
            for line in blob_info:
                self.write(line+"<br>")
            
            #self.write("Hello")


            
                    








 










app = webapp2.WSGIApplication([('/admin/files/',MainHandler),
                               
                               ('/admin/files/serve/([^/]+)?', ServeHandler),
                               ('/admin/files/college',CollegeHandler),
                               ('/admin/files/subject',SubjectHandler),
                               ('/admin/files/dept',DeptHandler),
                               ('/admin/files/colldept',CollDeptHandler)
                               #('/admin/files/college/serve/([^/]+)?',CollegeServeHandler)
                               
                               ])
def main():
    app.run()
    

if __name__=='__main__':
    main()                               