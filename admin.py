import os

import webapp2
import jinja2
import urllib
import models






from google.appengine.ext import db
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)


"""
Google AppEngine Reference links
https://developers.google.com/appengine/docs/python/datastore/gqlreference
https://developers.google.com/appengine/docs/python/datastore/datamodeling#References
http://stackoverflow.com/questions/2277739/whats-the-raw-gql-to-check-a-referenceproperty
https://developers.google.com/appengine/docs/python/datastore/queries
https://developers.google.com/appengine/docs/python/datastore/queryclass#Query_filter


"""



def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

class ResultHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        return render_str(template, **params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))
        

        
class AdminPageHandler(ResultHandler):
    #Handler for /admin/ url. 
    #index page for admin panel.
    #Post function not necessary
    
    def get(self):
        self.render("adminindex.html")
    
    
    

class UnivHandler(ResultHandler):
    
    #UnivHandler - for handling /admin/AddUniv url. 
    #Add a university name and code to the datastore
    #univpage.html is the template file
    
    
    def get(self):
        
        #self.render takes a keyword paramter univ.
        #univ is the query containing all items of the Univ Entity
        #it is passed to the template
        
        query = db.GqlQuery("Select * from Univ")
        self.render("univpage.html",univ=query)
        
    
    def post(self):
        code = self.request.get('univcode')
        name = self.request.get('univname')
        u = models.Univ(name=name,key_name=code)
        u.put()
        self.write("Thanks! University Added")
        
        


class ListUnivHandler(ResultHandler):
    
    #ListUnivHandler- Handler for /admin/ListUniv url
    #Post function not necessary
    #Lists the Universities present in the datastore
    #listuniv.html is the template file used
    
    def get(self):
        #query contains all elements of Univ Entity.
        #query is passed to the template
        query = db.GqlQuery("Select * from Univ")
        self.render("listuniv.html",univ=query)
        

class CollegeHandler(ResultHandler):
    
    #CollegeHandler - handles /admin/AddCollege url.
    #Used to add a College name, code and also the University it belongs to.
    #Uses a ReferenceProperty for the university. 
    #addcoll.html is the template used.
    
    def get(self):
        #univ is the keyword parameter. It contains the query result of all items in the Univ entity 
        #univ is passed to the template
        univ = db.GqlQuery("Select * from Univ")
        self.render("addcoll.html",univ=univ)
           
    def post(self):
    
        collname = self.request.get('collname')
        collcode = self.request.get('collcode')
        univname = self.request.get('univ')
        
        #self.write("hello" + univname)
        
        #To specify the University in College Entity, we retrieve the key of the selected University 
        #Obtain the instance of a Univ using get(key)
        
        k = db.Key.from_path('Univ',univname)
        k2 = db.get(k)
        
        #self.write( " " + str(k))
        
        College = models.College(colname=collname,key_name=collcode)
        College.univcode = k2
        College.put()
        #self.write(k)
        
        self.write("Thanks! College Added! ")
        
        
       
        
class ListCollegeHandler(ResultHandler):
    
    #ListCollegeHandler- handler for /admin/ListCollege url.
    #Lists the colleges and their universities.
    #listcollege.html is the template file
    #post function not necessary
    
    def get(self):
        #coll contains all items in the College Entity.
        #coll is passed to the template
        
        coll = db.GqlQuery("Select * from College")
        self.render("listcollege.html",coll = coll)



class DeptHandler(ResultHandler):
    
    #DeptHandler - handler for /admin/AddDept url
    #template file is adddept.html
    
    def get(self):
        #dep contains all department objects present in the Department entity
        #dep is passed to the template file
        dep = db.GqlQuery("Select * from Department")
        
        self.render("adddept.html",dep=dep)
    
    def post(self):
        
        dcode = self.request.get('dcode')
        dname = self.request.get('dname')
        d = models.Department(key_name=dcode,depname=dname)
        d.put()
        self.write("Thanks! Department Added")



class ListDeptHandler(ResultHandler):
    
    #ListDeptHandler - handler for /admin/ListDept url
    #template file is listdept.html
    #post function not needed
    
    def get(self):
        #dep contains all department objects present in the Department entity.
        #dep is passed to the template file
        dep = db.GqlQuery("Select * from Department")
        self.render("listdept.html",dep = dep)
        

class CollDeptHandler(ResultHandler):
    
    #CollDeptHandler- handler for /admin/CollDept url
    #Assign department to a college 
    #template file used is colldept.html
    
    def get(self):
        
        #coll contains all objects of College Entity
        #dept contains all department objects present in the Department entity
        #coll and dept are passed to the template file
        coll = db.GqlQuery("Select * from College")
        dept = db.GqlQuery("Select * from Department")
        self.render("colldept.html",coll=coll,dept=dept)
        
    def post(self):
        
        collname = self.request.get('coll')
        deptname = self.request.get('dept')
        
        kname = collname+deptname
        c = db.Key.from_path('College',collname)
        coll = db.get(c)
        
        d = db.Key.from_path('Department',deptname)
        dept = db.get(d)
        
        colldept = models.CollDept(dept = dept,coll = coll,key_name=kname)
        colldept.put()
        
        self.write("Thanks! Assigned Department to College")
        
        

class ListCollDeptHandler(ResultHandler):
    
    #ListCollDeptHandler - Handler for /admin/ListCollDept url
    #Lists the college and the courses they offer
    #post function not needed
    
    def get(self):
        
        #que contains all objects in the CollDept entity.
       
        que = db.GqlQuery("Select * from CollDept")
        for each in que:
            self.write("<p>" + each.coll.colname + " "  + each.dept.depname)
        
        """
        This piece of code lists the college where IT department is present
        k = db.Key.from_path('Department','205')
        query = models.CollDept.all()
        query.filter('dept = ',k )
        answer = query.fetch(10)
        for each in answer:
            self.write("<p><p><p> "+each.coll.colname)
            
        
        """
            
class SubHandler(ResultHandler):
    #SubHandler - handler for /admin/AddSub url
    #Add subject to the datastore. Subject has a name, code(key_name), semester, department and credits
    #template file used is addsub.html
    
    def get(self):
        dept = db.GqlQuery("Select * from Department")
        self.render("addsub.html",dept=dept)
    
    def post(self):
        
        subcode = self.request.get('subcode')
        subname = self.request.get('subname')
        credit = int(self.request.get('credits'))
        
        #It is better to assign subjects to department separately because of some subjects being common to several depts
        #Using the same approach used in College-Department Relationship. A new Handler is created for this purpose.
        
        #The semester of the subject varies with respect to the department usually for any University. (eg) Anna University
        #Hence it is not used as a propery here.
        
        #sem = self.request.get('sem')
        
        #d = self.request.get('dept')
        #elective = self.request.get('elective')
        #depkey = db.Key.from_path('Department',d)
        #dept = db.get(depkey)
        
        #Not necessary since new handler is used for subject-department relationship.
        #s.dept = dept
        #elective dynamic property is also removed because a subject can be elective for one department but regular subject for 
        #other department.
        #if elective == "on":
        #   s.elective = "yes"
        
        
        s = models.Subject(key_name=subcode,subname=subname,credits=credit)
        s.put()

        self.write("Thanks! Subject Added")
        

class ListSubHandler(ResultHandler):
    #ListSubHandler - Handler for /admin/ListSub url
    #Lists the Subjects in the datastore. 
    #Lists the Name, Subject Code and credits
    #post function not necessary
    #template file used - listsub.html
    
    def get(self):
        #subjects contains all the items in Subject Entity.
        #Used as a parameter in template
        subjects = db.GqlQuery("Select * from Subject ")
        self.render("listsub.html",sub=subjects)



class RegHandler(ResultHandler):
    
    #Handler for /admin/AddReg url
    #template file used is addreg.html
    
    def get(self):
        reg = db.GqlQuery("Select * from Regulation") 
        self.render("addreg.html",reg=reg)
    
    def post(self):
        
        
        reg = self.request.get('reg')
        r = models.Regulation(key_name=reg,name=reg)
        r.put()
        self.write("Thanks! Regulation Added")
        

class YearofJoinHandler(ResultHandler):
    #Handler for /admin/AddYoJ url
    #template file used is addyoj.html
    
    def get(self):
        
        yoj = db.GqlQuery("Select * from YearofJoining") 
        self.render("addyoj.html",yoj=yoj)
    
    def post(self):
        
        yoj = self.request.get('yoj')
        if len(yoj ) == 1:
            yoj = '0'+yoj
        desc = "Year"+yoj
        r = models.YearofJoining(key_name=yoj,desc=desc)
        r.put()
        self.write("Thanks! Year of Joining Added")
        


class UnivRegHandler(ResultHandler):
    #Handler for /admin/UnivReg url
    #template file used is univreg.html
    
    def get(self):
        
        univ = db.GqlQuery("select * from Univ")
        reg  = db.GqlQuery("Select * from Regulation")
        self.render("univreg.html",univ=univ,reg=reg)
        
    
    def post(self):
        
        univ = self.request.get('univ')
        reg = self.request.get('reg')
        kvalue = univ+"_"+reg
        
        ukey = db.Key.from_path('Univ',univ)
        u = db.get(ukey)
        
        rkey = db.Key.from_path('Regulation',reg)
        r = db.get(rkey)
        
        univreg = models.UnivReg(univ = u,reg= r,key_name=kvalue)
        univreg.put()
        
        self.write("Thanks! Added Regulation to University")
        


class RegYearHandler(ResultHandler):
    #Handler for /admin/RegYear url
    #template file used is regyear.html
    
    def get(self):
        
        year = db.GqlQuery("select * from YearofJoining")
        reg  = db.GqlQuery("Select * from Regulation")
        self.render("regyear.html",yoj=year,reg=reg)
        
    
    def post(self):
        
        yoj = self.request.get('yoj')
        reg = self.request.get('reg')
        kvalue = yoj+"_"+reg
        
        ykey = db.Key.from_path('YearofJoining',yoj)
        y = db.get(ykey)
        
        rkey = db.Key.from_path('Regulation',reg)
        r = db.get(rkey)
        
        yearreg = models.RegYear(year=y,reg=r,key_name=kvalue)
        yearreg.put()
        
        self.write("Thanks! Added YearofJoining to Regulation")
        


class SubDeptHandler(ResultHandler):
    #SubDeptHandler - handler for /admin/SubDept url.
    #Add Subject, Sem, Department, RegUniv
    #template file used is subdept.html
    
    def get(self):
        #dept contains all items in Department Entity
        #it is passed to the template file.
        
        sub = db.GqlQuery("Select * from Subject")
        dept = db.GqlQuery("Select * from Department")
        reguniv = db.GqlQuery("Select * from UnivReg")
        self.render("subdept.html",dept=dept,sub=sub,reguniv=reguniv)
        
    def post(self):
        
        sub = self.request.get('sub')
        dept = self.request.get('dept')
        sem = self.request.get('sem')
        reguniv = self.request.get('reguniv')
        elective = self.request.get('elective')
        
        kvalue = sub+"_"+dept
        skey = db.Key.from_path('Subject',sub)
        s = db.get(skey)
        dkey = db.Key.from_path('Department',dept)
        d = db.get(dkey)
        rukey = db.Key.from_path('UnivReg',reguniv)
        ru = db.get(rukey)
        
        sd = models.SubDept(sub=s,dept=d,sem=sem,reguniv=ru,key_name=kvalue)
        
        if elective == "on":
            sd.elective = "Yes"
        
        sd.put()
        

        self.write("Thanks! Subject added to the Department")
        


class ListSubDeptHandler(ResultHandler):
    #Lists the Subjects assigned to Department along with Semesters and Regulations
    #post function not needed
    #template file used is listsubdept.html
    
    def get(self):
        
        subdept = db.GqlQuery("Select * from SubDept")
        self.render("listsubdept.html",subdept=subdept)
        
            
        


class SearchHandler(ResultHandler):
    pass
    



class TestHandler(ResultHandler):
    #handler for testing purposes
    
       
    def get(self):
        dep = db.GqlQuery("Select * from Department")
        ru = db.GqlQuery("Select * from UnivReg")
        
        
        self.render("testq.html",dept=dep,reguniv=ru,res='')
    
    def post(self):
        
        """
        This piece of code lists the college where IT department is present
        k = db.Key.from_path('Department','205')
        query = models.CollDept.all()
        query.filter('dept = ',k )
        answer = query.fetch(10)
        for each in answer:
            self.write("<p><p><p> "+each.coll.colname)
            
        
        """
        
        """
        #self.write("Subject of IT department under Regulation 2008 ")
        k = db.Key.from_path('UnivReg','1_2008')
        d = db.Key.from_path('Department','205')
        query = models.SubDept.all()
        query.filter('dept =',d)
        #depquery = query.filter('dept = ',d)
        #dep_reg_query = depquery.filter('reguniv = ',k)
        #a = dep_reg_query.fetch(10)
        #for each in a:
        #    self.write("<p><p>" + each.sub.subname + " " + each.sem + "</p></p>")
        
        self.write("Subject of IT department")
        b = query.fetch(10)
        for each in b:
            self.write("<p><p>" + each.sub.subname + "</p></p>")
        
        self.write("Subjects under Reg 2008  - IT")
        query.filter('reguniv = ',k)
        c = query.fetch(10)
        for each in c:
            self.write("<p><p>" + each.sub.subname + "</p></p>")
            
            
        
        """
        
        d = self.request.get('dept')
        r = self.request.get('reg')
        sem = self.request.get('sem')
        dkey = db.Key.from_path('Department',d)
        rukey = db.Key.from_path('UnivReg',r)
        
        query = models.SubDept.all(keys_only=True)
        
        if sem == '':
            query.filter('dept = ',dkey).filter('reguniv = ',rukey)
        else:
            query.filter('dept = ',dkey).filter('reguniv = ',rukey).filter('sem =',sem)
        #res = query.fetch(10)
        
        res = db.get(query.fetch(10))
        
        dep = db.GqlQuery("Select * from Department")
        ru = db.GqlQuery("Select * from UnivReg")
        self.render("testq.html",dept=dep,reguniv=ru,res=res)
        
        
        
             
        
class TestRegNumHandler(ResultHandler):
    
    #TestRegNumHandler - handler for RegNum Test 
    #template file used is testreg.html
    
    def get(self):
        
        self.render("testreg.html",res=[],res_exist=0,error='')
    
    
    def post(self): 
        
        regnum = self.request.get('regno')
        res = list()
        if len(regnum) == 12 and regnum.isdigit():
            colcode = regnum[0:4]
            yoj = regnum[4:6]
            dept= regnum[6:9]
            rolno = regnum[9:]
        
        else:
            self.render("testreg.html",res=[],res_exist=0,error="Invalid RegisterNumber")
            return 
        
        
        colkey = db.Key.from_path('College',colcode)
        coll = db.get(colkey)
        
        
        
        
        year = db.Key.from_path('YearofJoining',yoj)
        q = models.RegYear.all()
        q.filter('year = ',year)        
        b = q.fetch(1)
        
        for each in b:
            
            regulation = each.reg
        
        univcode = coll.univcode
        
        query = models.UnivReg.all(keys_only=True).filter('reg = ',regulation).filter('univ = ',univcode)
        runiv = query.fetch(1)
        
        for each in runiv:
            #self.write(each.key().name())
            #ru = db.Key.from_path('UnivReg',each.key().name())
            
            ru = db.get(each)
            #self.write(ru.reg.name)
        
        
        
        
        
        res.append(coll)
        
        dkey = db.Key.from_path('Department',dept)
        d = db.get(dkey)
        dep = d.depname
        
        
        
        
        
        
        sub = models.SubDept.all(keys_only=True)
        sub.filter('dept = ',dkey).filter('reguniv = ',ru)
        sub.order('sem')
        
        #subjects = sub.fetch(10)
        
        #subj = sub.fetch(10)
        
        subjects = db.get(sub)
        
        self.render("testreg.html",res=res,res_exist=1,regyear=b,dept=dep,error='',subjects=subjects)
        
        
    



            



app = webapp2.WSGIApplication([('/admin/',AdminPageHandler),
                               ('/admin/AddUniv',UnivHandler),
                               ('/admin/ListUniv',ListUnivHandler),
                               ('/admin/AddCollege',CollegeHandler),
                               ('/admin/ListCollege',ListCollegeHandler),
                               ('/admin/AddDept',DeptHandler),
                               ('/admin/ListDept',ListDeptHandler),
                               ('/admin/CollDept',CollDeptHandler),
                               ('/admin/ListCollDept',ListCollDeptHandler),
                               ('/admin/AddSub',SubHandler),
                               ('/admin/ListSub',ListSubHandler),
                               ('/admin/SubDept',SubDeptHandler),
                               ('/admin/AddReg',RegHandler),
                               ('/admin/AddYoJ',YearofJoinHandler),
                               ('/admin/UnivReg',UnivRegHandler),
                               ('/admin/RegYear',RegYearHandler),
                               ('/admin/ListSubDept',ListSubDeptHandler),
                               ('/admin/Search',SearchHandler),
                               ('/admin/TestQuery',TestHandler),
                               ('/admin/TestRegNum',TestRegNumHandler)
                            
                               
                               ],
                              debug=True)
def main():
    app.run()
    

if __name__=='__main__':
    main()