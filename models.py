from google.appengine.ext import db

class Univ(db.Model):
    #code is used as Key Name. Hence not needed as a separate property
    
    name = db.StringProperty()
    #code = db.StringProperty()

class Regulation(db.Model):
    #RCode is used as a key name
    name = db.StringProperty()
    
    

class College(db.Model):
    #colcode is used as Key Name. Hence not needed as a separate property
    univcode = db.ReferenceProperty(Univ)
    colname = db.StringProperty()
    
    
    #colcode = db.StringProperty()


class Department(db.Model):
    #depcode is used as Key Name.
    depname = db.StringProperty()

class Subject(db.Expando):
    #subcode is used as Key Name. Hence not needed as a separate property.
    #dept reference property is removed because a new relationship is used.
    #subcode = db.StringProperty()
    #dept = db.ReferenceProperty(Department)
    #semester = db.StringProperty()
    
    subname = db.StringProperty()
    credits = db.IntegerProperty()


class UnivReg(db.Model):
    # Key Name is Univcode + Reg.
    
    univ = db.ReferenceProperty(Univ)
    reg = db.ReferenceProperty(Regulation)
    
class YearofJoining(db.Model):
    #Year can be considered as the Key Name.
    #year = db.StringProperty()
    
    desc = db.StringProperty()
    
class RegYear(db.Model):
    #Key Name =
    year = db.ReferenceProperty(YearofJoining)
    reg = db.ReferenceProperty(Regulation)
    
class CollDept(db.Model):
    dept = db.ReferenceProperty(Department, collection_name="dept_details")
    coll = db.ReferenceProperty(College, collection_name="college_details")

class SubDept(db.Expando):
    
    #reg = db.ReferenceProperty(Regulation)
    #KeyName is the problem here. Think of some solution.
    
    reguniv = db.ReferenceProperty(UnivReg)
    dept = db.ReferenceProperty(Department)
    sem = db.StringProperty()
    sub = db.ReferenceProperty(Subject)
    


class Result(db.Expando):
    
    regno = db.StringProperty()
    name =  db.StringProperty()
    coll_depname = db.StringProperty()
    colldept = db.ReferenceProperty(CollDept)
    cyd = db.StringProperty()
    #yoj = db.ReferenceProperty(YearofJoining)
    
    cyd_sem = db.StringProperty()
    gpa = db.StringProperty()


class InvalidNum(db.Expando):
    
    #cyd (first 9 or 8 digits of regno) is used as key_name
    
    missingnumbers = db.TextProperty()



class DeptResult(db.Expando):
    
    status = db.StringProperty()
    dept = db.StringProperty()
    