from __future__ import division
import os
import webapp2
import operator
import jinja2
import models
from collections import Counter
from bs4 import BeautifulSoup
from google.appengine.ext import db
from google.appengine.api import urlfetch
from google.appengine.api import taskqueue
from google.appengine.api import memcache
from google.appengine.api import logservice
from google.appengine.datastore import entity_pb


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True,extensions=["jinja2.ext.do",])


#############################3  Useful Functions #####################################

def serialize_entities(models):
    if models is None:
        return None
    elif isinstance(models, db.Model):
        # Just one instance
        return db.model_to_protobuf(models).Encode()
    else:
        # A list
        return [db.model_to_protobuf(x).Encode() for x in models]

def foo(each,s):
    return getattr(each,s)

def deserialize_entities(data):
    if data is None:
        return None
    elif isinstance(data, str):
        # Just one instance
        return db.model_from_protobuf(entity_pb.EntityProto(data))
    else:
        return [db.model_from_protobuf(entity_pb.EntityProto(x)) for x in data]
    
def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)


############################# END of USEFUL FUNCTIONS ################################ 



class ResultHandler(webapp2.RequestHandler):
    
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)
        
    def render_str(self, template, **params):
        return render_str(template, **params)

    def render(self, template, **kw):
        html_page = self.render_str(template,**kw)
        #self.write(self.render_str(template, **kw))
        self.write(html_page)
        return html_page
    

        

class MainPage(ResultHandler):
    #handler for / url
    #Mainpage of the webapplication
    
    def gpa(self,sub_cred_dict):
        
        grade_pts = { 'S' : 10, 'A' : 9, 'B' : 8, 'C' : 7, 'D' : 6, 'E' : 5, 'U' : 0,'W' : 0 , 'I': 0, 'UA' : 0,'WH1' : 0,'WH2' : 0, 'WH3':0, 'WH4': 0 ,'WH5': 0, 'WH6' : 0, 'WH7' : 0, 'WH8': 0, 'WH9' : 0 , 'WH10': 0 , 'WH11': 0, 'SA' : 0,'SE':0}
        total_pts = 0
        cred_acq = 0
        
        for (g,c) in sub_cred_dict.values():
            if  g not in ('UA','U','WH','WH1','WH2','WH3','WH4','WH5','WH6','WH7','WH8','WH9','WH10','WH11'):
                total_pts = total_pts + (grade_pts[g] * c)
                cred_acq = cred_acq + c
        if cred_acq != 0:
            gpa = total_pts / float(cred_acq)
            return "%.3f" % (gpa)
        else:
            return "N/A"
        
        
    
    
    def extract_year_for_s9(self,regno,year=None,exam_held_month=None):
        
        if exam_held_month is None:
            year = memcache.get('s9year')
            exam_held_month = memcache.get('s9examheldmon')
            
            if year is None:
                url = "http://schools9.com/anna-university-grade-system-chennai.htm"
                contents = urlfetch.fetch(url)
                soup =  BeautifulSoup(contents.content)
                titleTag = soup.html.head.title
                yearStr  = titleTag.string
                yearStr = yearStr.split("|")[1].split(" ")
                #self.write(yearStr)
                year = yearStr[4]
                #self.write(year)
                exam_held_month = yearStr[3].split('/')[0]
                #self.write(exam_held_month)
                memcache.set('s9year',year)
                memcache.set('s9examheldmon',exam_held_month)
            
        
        if len(regno) == 12:
            key_sem = int(year[2:4]) - int(regno[4:6])
        elif len(regno) == 11:
            key_sem = int(year[2:4]) - int(regno[3:5])
        
        if exam_held_month in ('Jan','Feb','Mar','Apr','May','June'):
            sem_type = "even"
        else:
            sem_type = "odd"
        
        if sem_type == "even":
            key_val =  str(key_sem) + "_" + str(2)
        else:
            key_val =  str(key_sem) + "_" + str(1)
        
        #self.write(key_val)
        return key_val
        
    
    
        
    
    def fetch_from_s9(self,regno):
        
        #url = "http://schools9.com/anna-university-grade-system-chennai.aspx?htno="+regno
        url = "http://schools9.com/anna-university-grade-system-exam-results200114.aspx?htno="+regno
        contents = urlfetch.fetch(url,deadline=10)
        soup = BeautifulSoup(contents.content)
        data = soup.get_text("|", strip=True)
        
        if data == '':
            return None,None
        
        results = data.split('|')[3:]
        
        if len(results)!=0:
            results.remove('Course')
            results.remove('Marks Details')
            results.remove('Grade')
            results.remove('Subject')
            results.remove('Status')
        
        #self.write(results)
        
        
        
        key_val = self.extract_year_for_s9(regno,exam_held_month='Nov',year='2014')
        
        
        return results,key_val
        
                
        
    def extract_year(self,regno,restext):
        exam_res_year = restext.split('UG/PG Results')[1]
        exam_res_year = exam_res_year.split(' ')
        
        #exam_held_month = exam_res_year[1]
        exam_held_month = exam_res_year[1].split('/')[0]
        #year = exam_res_year[4].split("|")[0]
        year = exam_res_year[2].split("|")[0]
        
        #self.write(year)
        
        if len(regno) == 12:
            key_sem = int(year[2:4]) - int(regno[4:6])
        elif len(regno) == 11:
            key_sem = int(year[2:4]) - int(regno[3:5])
            
            
        
        if exam_held_month.capitalize() in ('Jan','Feb','Mar','Apr','May','June'):
            sem_type = "even"
        else:
            sem_type = "odd"
        
        if sem_type == "even":
            key_val =  str(key_sem) + "_" + str(2)
        else:
            key_val =  str(key_sem) + "_" + str(1)
        
        return key_val
    
  
    
    
    def fetch_from_au(self,regno):
        
        #contents = urllib2.urlopen("http://www.annauniv.edu/cgi-bin/result/cgrade.pl?regno="+regno)
        
        #contents = urlfetch.fetch("http://www.annauniv.edu/cgi-bin/result/cgrade.pl?regno="+regno,deadline=10)
        contents = urlfetch.fetch("http://result.annauniv.edu/cgi-bin/divum/grad1414g1.pl?regno="+regno,deadline=10)
        
        soup = BeautifulSoup(contents.content)
        data = soup.get_text("|",strip=True)
        
        
        if 'is wrong!' in data:
            return None,None
        
        #Extract year info for key_name to be put in datastore. 
        #year info is the information about which semester exams for the student. 
        #example if the results are for May/June 2013 and student's regno is 310611205011. Year of Join = 11
        #May/June results is for even sem. So, 2nd year 2nd Sem.
        
        key_val = self.extract_year(regno, data)
        
        #self.fetch_from_s9(regno)
        #self.extract_year_for_s9(regno)
        #Extract the Result Portion alone
        
        
        
        results = data.split(regno)[1].split('Click here for Legends')[0]
        
    
        results = results.split("|") #make it a list from a string.
        
        results.pop()   #To remove last element from list. It is a blank character for the above extraction method used.
        
        if len(results) != 0:
            results.pop(0)  #To remove first element from list. It is a blank character
            results.remove('Subject Code') 
            results.remove('Grade')
            results.remove('Result')
            
        else:
            #This case might occur when the register number is 'valid' but no student exists for that register Number.
            #In this case, length of register number will be 12. But no result because college code might be invalid or any part
            #of the Register Number might not be valid such as a particular department does not exist for that college.
            #Hence returning a None Object.
            
            return None,None       
        
        #Return the results.
        
        #self.write(results)
        return results,key_val
    
        
    
    def process_result(self,regno,results,info,key_val):
        
        subjects = []
        grades = []
        #result_list = []
        
        if any('WH' in r for r in results):
            
            #self.write("Withheld Case to be handled")
            #self.write(results)
            subjects = results[2::2]
            #self.write(subjects)
            grades = results[3::2]
            #self.write(grades)
        
        elif any('SA' in r for r in results):
            res_dup = [ x for x in results[2::] if x not in ['RA','PASS']]
            
            for (sub,g) in zip(res_dup[::2],res_dup[1::2]):
                subjects.append(sub)
                grades.append(g)
                #self.write(subjects)
                #self.write(grades)
                
        elif any('SE' in r for r in results):
            res_dup = [ x for x in results[2::] if x not in ['RA','PASS']]
            
            for (sub,g) in zip(res_dup[::2],res_dup[1::2]):
                subjects.append(sub)
                grades.append(g)
                #self.write(subjects)
                #self.write(grades)       
            
        
        else:
            #self.write(results)
            subjects = results[2::3]
            #self.write(subjects)
            grades = results[3::3]
            #self.write(grades)
        
        
        
        
        
        #### This portion should be optimized !! ### ###################### 
        """ This portion is not actually used by any part of the code. So Commented as of now.
        subnames = list()
        skeylist= list()
        for each in subjects:
            skey = db.Key.from_path('Subject',each)
            skeylist.append(skey)  #Finding subject names 
        
        subnames = db.get(skeylist) #Retrieve subjects names as a batch get from datastore.
        """ 
        #### #######################################################   ####
        
        
        # Try doing this operation separately and passing the result to this method to avoid retrival repeatedly.
        
        k = memcache.get('CollDept'+info[0]+info[2])
        
        if k is None:
        
            k = db.Key.from_path('CollDept',info[0]+info[2]) #info is a tuple containing extracted information of the regno
            memcache.set('CollDept'+info[0]+info[2],k)
    
        r = models.Result(regno=regno, key_name = regno + "_" + key_val)
        
        if len(regno) == 12:
            r.cyd = regno[0:9]
        elif len(regno) == 11:
            r.cyd = regno[0:8]
            
        r.cyd_sem = r.cyd + "_" + key_val
        
        r.name = results[0]
        r.colldept = k
        collname = memcache.get(info[0])
        
        if collname is None:

            collname  = db.get(k).coll.colname
            memcache.set(info[0],collname)
            
            
        r.coll_depname = collname+'  '+results[1] #k is colldept key. So using that to extract college name
        
        self.write("<br>Student Name: " + results[0])
        self.write("<br>Department: " + results[1])
        self.write("<br>College: "+collname + "<br>")
        
        """
        result_list.append(regno)
        result_list.append(results[0])
        result_list.append(results[1])
        result_list.append(collname)
        
        """
        sub_cred_dict = dict()
        
        self.write('<table style="border: 1px solid black">')
        self.write('<th>S.No </th> <th>Subject Code </th><th>Subject Name </th> <th>Grade </th> <th> Result </th><th> Semester </th>')
        for pos,(sub,grade) in enumerate(zip(subjects,grades),start=1):
            
            credits = memcache.get(sub+"_"+info[2]+"cred")
            subname = memcache.get(sub+"_"+info[2]+"subname")
            sem = memcache.get(sub+"_"+info[2]+"sem")
            
            
            if credits is None or subname is None:
                sdkey = db.Key.from_path('SubDept',sub+"_"+info[2])
                sd = db.get(sdkey)
                sem = sd.sem
                credits = sd.sub.credits
                subname = sd.sub.subname
                memcache.set(sub+"_"+info[2]+"sem",sem)
                memcache.set(sub+"_"+info[2]+"cred",credits)
                memcache.set(sub+"_"+info[2]+"subname",subname)
                
            
            #result_list.append( (sub,subname,grade,sem) ) 
            
            
            #setattr(r,sub,sd.sem+'  '+str(sd.sub.credits)+'  '+grade+'  '+sd.sub.subname)
            
            setattr(r,sub,sem+'  '+str(credits)+'  '+grade+'  '+subname)
            
            
            self.write('<tr>')
            
            if 'WH' in grade:
                self.write("<td>"  + str(pos) + "</td><td>"+ sub + "</td><td>" + subname + "</td><td> </td>" + "<td>" +  grade + "</td><td>" + sem + "</td>")
            
            elif 'SA' not in grade:
                if grade == 'UA' or grade == 'U':
                    res_status = "RA"
                else:
                    res_status = "PASS"
                
                #self.write("<td>"+sub + "</td><td> " + sd.sub.subname+ " </td><td>" + grade + " </td><td>" + res_status+ "</td><td>" + sd.sem + "</td>")
                self.write("<td>"+ str(pos) + "</td><td>" + sub + "</td><td> " + subname+ " </td><td>" + grade + " </td><td>" + res_status+ "</td><td>" + sem + "</td>")
                
                
            else:
                #self.write("<td>"+sub+ "</td><td>" + sd.sub.subname + "</td><td> </td> <td> "+ grade + "(Shortage of Attendance) </td>" + "<td> " + sd.sem + "</td>")
            
                self.write("<td>"+ str(pos) + "</td><td>" + sub+ "</td><td>" + subname + "</td><td> </td> <td> "+ grade + "(Shortage of Attendance) </td>" + "<td> " + sem + "</td>")
            
            
            
            #sub_cred_dict[sd.sub] = grade,sd.sub.credits
            sub_cred_dict[sub] = grade,credits
            self.write('</tr>')
       
        gpa = self.gpa(sub_cred_dict)
        self.write("</table><b>GPA:" +str( gpa) + "</b><br>")      
        r.gpa = gpa
        #result_list.append(gpa)
        
        #return r
        r.put()
        
        #return result_list
        
        
        
        
                
                
    def get(self):
        self.render("index.html",error='')
        
        #template = jinja_env.get_template('index.html')
        #output_html = template.render(error='')
        #self.write(output_html)
        
        
    def post(self):
        #Results list format: results = [Student Name,degree and dept,s1,g1,p/f,s2,g2,p/f......sn,gn,p/f]
        main_results_list = []
        results_list = []
        regno  = self.request.get('regno')
        self.write("<!DOCTYPE html>")
        self.write('<html><head><title>AbsCool </title><style type="text/css">')
        self.write("table, td, th{border:1px solid black;}")
        self.write("</style></head><body>")
        #self.write("<h2> AbsCool - Results </h2>")
        
        #Extract information from regno (Register Number), input by the user.
        
        if len(regno) == 12:
            collcode = regno[0:4]
            yearofjoin = regno[4:6]
            dept = regno[6:9]
            rollno = regno[9:]
            info = (collcode,yearofjoin,dept,rollno)
        elif len(regno) == 11:
            collcode = regno[0:3]
            yearofjoin = regno[3:5]
            dept = regno[5:8]
            rollno = regno[8:]
            info = (collcode,yearofjoin,dept,rollno)
            
        
        else:
            self.render("index.html",error="Invalid RegisterNumber")
            return
            
        ### Extraction of Regno over. ###
            
        
        #Fetch Result from Source and store it in results variable.
        
        query = models.Result.all().filter('regno = ',regno).order('-__key__')
        rows  = db.Query.fetch(query,1)
        
        #If len(rows) > 0, then fetched from datastore
        
        if len(rows) > 0:
            self.write("Fetched from datastore")
            
            
            for each in rows:
                coll_dept = each.coll_depname.split('  ')
                key_val = each.key().name()
                keyvalue = key_val.split('_')
                keyvalue = keyvalue[1]+"_"+keyvalue[2]
                
                """
                results_list.append(regno)
                results_list.append( each.name)
                
                results_list.append(coll_dept[1])
                results_list.append(coll_dept[0])
                """
                
                
                self.write("<br>Student Name: " + each.name)
                self.write("<br>RegisterNum: " + regno)
                self.write("<br>College Name: " + coll_dept[0])
                self.write("<br>Department: "+ coll_dept[1])
                self.write("<br>")
                prop = each.dynamic_properties()
                semlist = []
                
                for p in prop:
                    
                    semlist.append( getattr(each,p).split('  ')[0] )
                    
                #self.write(semlist)
                
                semdict = { sub : sem for (sub,sem) in zip(prop,semlist) }
                
                #self.write(semdict)
                
                sorted_semdict = sorted(semdict.iteritems(),key=operator.itemgetter(1),reverse=True)
                
                #self.write(sorted_semdict)
                
                self.write('<table style="border: 1px solid black">')
                self.write('<th>S.No </th><th>Subject Code </th><th>Subject Name </th> <th>Grade </th> <th> Result </th><th> Semester </th>')
                for pos,(p,sem) in enumerate(sorted_semdict,start=1):
                    self.write('<tr>')
                    self.write("<td> " + str(pos)+ "</td><td>" + p + "</td> ")
                    s = getattr(each,p).split('  ')
                    #results_list.append( (p,s[3],s[2],s[0]) )
                    if 'SA' not in s[2]:
                        if s[2] == 'UA' or s[2] == 'U':
                            res_status = "RA"
                        else:
                            res_status = "PASS"
                        self.write("<td>" + s[3]+ "</td><td>" + s[2] + "</td><td>" + res_status + "</td><td>" + s[0] + "</td> " )
                    else:
                        self.write("<td>" + s[3]  + "</td><td> </td> <td> "+ s[2] + "(Shortage of Attendance) </td>" + "<td> " + s[0] + "</td>")
                    self.write('</tr>')
                    
                self.write("</table><br> <b> GPA: "+ str(each.gpa) + "</b><br>")
            
                #results_list.append(each.gpa)
                #main_results_list.append(results_list)
                #self.render("resultsdisp.html",results_list=main_results_list)
            
            
                        
            
            
        else:
            
            #fetching from AU site ( now the only source is AU). So modify this portion to 
            #handle many sources. Also, when fetching from some source, u need to process results and put it in 
            #datastore
            
            results,key_val = self.fetch_from_au(regno)
            #results,key_val = self.fetch_from_s9(regno)
            
            
            
            if results is not None:
            #    #self.write(results)
                #self.write(results) 
                keyvalue = key_val
                #results_list = self.process_result(regno,results,info,key_val)
                
                self.process_result(regno, results, info, key_val)
                
                #main_results_list.append(results_list)
                #self.render("resultsdisp.html",results_list = main_results_list)
                #res_entity.put()
                
            else:
                
                #No Results available for this Number. 
                #This may be because of student dropping out.
                
                self.render("index.html",error="No Results Available for this Register Number.")
                return
        
        
        
        if len(regno) == 12:
            reg = regno[0:9]
        elif len(regno) == 11:
            reg = regno[0:8]
        
        
        
        self.write('<div id="divdisp"></div>')
        dept_status = memcache.get(reg+"_"+keyvalue+"dept")
        
        if dept_status:
            self.write("Memcached")
            self.write('<br><a href="/dept/%s"> Click here to view Dept Results </a> ' %(reg))
        
        else:
            
            k = models.DeptResult.get_by_key_name(reg+"_"+keyvalue)
            #self.write(keyvalue)
            if k is None:
                #self.write("Coming here..")
                try:
                    taskqueue.add(url='/dept/%s' % (reg),method='GET',name='%s' % (reg) )
                except taskqueue.TaskAlreadyExistsError:
                    pass
                    
                
                self.write("Department Results being prepared.")
                self.write('<br><a href="/dept/%s"> Click here to view Dept Results </a> ' %(reg))
                
                
                #test_var = memcache.get(reg+"_"+keyvalue+"dept")
                
                #while test_var is None:
                #    test_var = memcache.get(reg+"_"+keyvalue+"dept")
                #    self.write("<script type='text/javascript'>document.getElementById('divdisp').innerHTML = %s</script>" % 'Getting ready...')
                
            
            else:
                self.write("Key missed")
                self.write('<br><a href="/dept/%s"> Click here to view Dept Results </a> ' %(reg))
            
            
            
            
        
    #taskqueue.add(url='/dept/%s' %(reg),method='GET')
        
    #self.write("<script type='text/javascript'>document.getElementsByTagName('html')[0].innerHTML </script>")
         
    #self.write('<br><a href="/dept/%s"> Click here to view Dept Results </a> ' %(reg))


class DeptViewHandler(MainPage):
    #Handler for dept view of the results
    
    
    def __init__(self,request,response):
        
        self.initialize(request, response)
        self.invalidnumbers = ''
        self.key_val = ''
        #self.main_results_list = []
        urlfetch.set_default_fetch_deadline(20)
        
        
    def handle_result(self,rpc,r,info):
        contents = rpc.get_result()
        
        if contents.status_code == 200:
            soup = BeautifulSoup(contents.content)
            data = soup.get_text("|",strip=True)
        
        if 'is wrong!' in data:
            results  = None
            key_val = None
        
        #Extract year info for key_name to be put in datastore. 
        #year info is the information about which semester exams for the student. 
        #example if the results are for May/June 2013 and student's regno is 310611205011. Year of Join = 11
        #May/June results is for even sem. So, 2nd year 2nd Sem.
        
        else:
            key_val = self.extract_year(r, data)
            self.key_val = key_val
        #self.fetch_from_s9(r)
        #self.extract_year_for_s9(r)
        #Extract the Result Portion alone
        
        
        
            results = data.split(r)[1].split('Click here for Legends')[0]
        
    
            results = results.split("|") #make it a list from a string.
        
            results.pop()   #To remove last element from list. It is a blank character for the above extraction method used.
        
            if len(results) != 0:
                results.pop(0)  #To remove first element from list. It is a blank character
                results.remove('Subject Code') 
                results.remove('Grade')
                results.remove('Result')
            
                
        
       
        if results is not None:
            self.process_result(r, results, info, key_val)
        
        else:
            #self.write("<br>No Results for "+r)
            if len(r) == 12:
                self.invalidnumbers = self.invalidnumbers + r[9:] + ' '
            elif len(r) == 11:
                self.invalidnumbers = self.invalidnumbers + r[8:] + ' ' 
            
        
        
    
    
    def handle_result_s9(self,rpc,r,info):
        
        contents = rpc.get_result()
        logservice.logging.info("Contents fetching")
        
        if contents.status_code == 200:
            
            soup = BeautifulSoup(contents.content)
            data = soup.get_text("|", strip=True)
            
        
        
        if data == '':
            results = None
            key_val = None
            #return None,None
        
        else:
            results = data.split('|')[3:]
        
            if len(results)!=0:
                results.remove('Course')
                results.remove('Marks Details')
                results.remove('Grade')
                results.remove('Subject')
                results.remove('Status')
        
        #self.write(results)
            #key_val = self.extract_year_for_s9(r,year='2014',exam_held_month='Nov') # year and exam details are optional parameters
            key_val = self.extract_year_for_s9(r,exam_held_month='Nov',year='2014')
            
            self.key_val = key_val
                
        #return results,key_val
        
        if results is not None:
            #self.write(results)
            self.process_result(r, results, info, key_val)
            
            
            #r_entity_list.append(r_entity)
            #
            
            #self.main_results_list.append(results_list)
            #self.render("resultsdisp.html",results_list =self.main_results_list)
            #self.main_results_list = []
            #self.main_results_list.append(results_list)
                    
        else:
            #self.write("<br>No Results for "+r)
            if len(r) == 12:
                self.invalidnumbers = self.invalidnumbers + r[9:] + ' '
            elif len(r) == 11:
                self.invalidnumbers = self.invalidnumbers + r[8:] + ' '        

    
        
        
        
    
    
    
    
    
    def create_callback(self,rpc,r,info):
        return lambda: self.handle_result(rpc,r,info)
        #return lambda: self.handle_result_s9(rpc, r, info)
    
    
    def get(self,reg):
        urls = []
        rpcs = []
        regnos = []
        invalidnumbers = ''
        regkeys = []
        r_entity_list = []
        results_list = []
        
        if len(reg) == 9:
            collcode = reg[0:4]
            yearofjoin = reg[4:6]
            dept = reg[6:9]
            info_reg = (collcode,yearofjoin,dept)
            
        elif len(reg) == 8:
            collcode = reg[0:3]
            yearofjoin = reg[3:5]
            dept = reg[5:8]
            
            info_reg = (collcode,yearofjoin,dept)
        
        
        
        """
        if len(regno) == 12:
            reg = regno[0:9]
            rollno = regno[9:]
        elif len(regno) == 11:
            reg = regno[0:8]
            rollno = regno[8:]
        """
        
        for i in range(1,1000):
            if len(str(i)) == 1:
                temp = "00" + str(i)
            elif len(str(i)) == 2:
                temp = "0" + str(i)
            else:
                temp = str(i)
            
            regnos.append(reg+temp)
            #urls.append("http://www.annauniv.edu/cgi-bin/result/cgrade.pl?regno="+reg+temp)    
            #regkeys.append(reg+temp+"_"+keyvalue)
        
        #self.write(regkeys)
        self.write("<br>")
        
        #self.key_val = self.extract_year_for_s9(regnos[0])
        
        self.key_val = self.extract_year_for_s9(regnos[0],year='2014',exam_held_month='May')
        
        #self.key_val = self.extract_year(regno, restext)
        
        ik = db.Key.from_path('InvalidNum',info_reg[0]+info_reg[1]+info_reg[2]+"_"+self.key_val)
        
        inv = db.get(ik)
        
        if inv is not None:
            inv_list = inv.missingnumbers.split(' ')
            
            #inv_list.pop(0)
        
            #self.write(inv_list)
            #self.write(rollno)
            #self.write(reg)
            for item in inv_list:
                if reg+item in regnos:
                    
                    regnos.remove(reg+item)
                    #regkeys.remove(reg+item+"_"+keyvalue)
                
        
        
        val = None
        
        
        #self.write(regnos)
        #self.write(regkeys)
        
        for (resultno,r) in enumerate(regnos,start=1):
            
            #k = db.Key.from_path('Result',rkey)
            #val = db.get(k)
            #k = memcache.get('query'+r)
            #if k is None:
            #k = memcache.get('query'+r)
            #if k is None:
            
            
            
            #k = models.Result.all(keys_only=True).filter('regno = ',r).order('-__key__').fetch(1)
            
            k = models.Result.all(keys_only=True).filter('regno = ',r).filter('cyd_sem = ',collcode+yearofjoin+dept+"_"+self.key_val).order('-__key__').fetch(1)
            
            
            #memcache.set('query'+r,db.get(k))

            
            
            
            
            
            
            if k:
                for each in k:
                    val = db.get(each)
                    
                    #val  = each
            else:
                val = None
        
            
            
            if val is not None:
                #self.write("Fetched from DS")
                #Fetch from Datastore if it exists
                #coll_dept is of the format: collname  deptname
                #each dynamic property is a subject. it is of the form: sem  cred  grade  subname
                
                coll_dept = val.coll_depname.split('  ')
                
                self.write("<br>" + str(resultno)+")   Student Name: " + val.name)
                #self.write("HTML = " + str(html))
                self.write("<br>Regno: " + val.regno)
                self.write("<br>College Name: " + coll_dept[0])
                self.write("<br>Department: "+ coll_dept[1])
                self.write("<br>")
                
                """
                
                results_list.append(val.regno)
                results_list.append(val.name)
                
                results_list.append(coll_dept[1])
                results_list.append(coll_dept[0])
                
                """
                
                
                prop = val.dynamic_properties()
                #self.write(str(prop)+"<br>")
                
                self.write('<table style="border: 1px solid black">')
                self.write('<th>Subject Code </th><th>Subject Name </th> <th>Grade </th> <th> Result </th><th> Semester </th>')
                
                
                for p in sorted(prop):
                    self.write("<tr><td>" + p + " </td>")
                    s = getattr(val,p).split('  ')
                    #self.write(s[3]+ "   " + s[2] + "  " + s[0])
                    #self.write("<br>")
                    
                    #results_list.append( (p,s[3],s[2],s[0]) )
                    
                    
                    if 'WH' in s[2]:
                        self.write("<td>"+ s[3] + "</td><td>" + "</td><td>" + s[2] + "</td><td>" + s[0] + "</td>")
                    
                    elif 'SA' not in s[2]:
                        
                        if s[2] == 'UA' or s[2] == 'U':
                            res_status = "RA"
                            
                        else:
                            res_status = "PASS"
                        self.write("<td>" + s[3]+ "</td><td>" + s[2] + "</td><td>" + res_status + "</td><td>" + s[0] + "</td> " )
                    else:
                        self.write("<td>" + s[3]  + "</td><td> </td> <td> "+ s[2] + "(Shortage of Attendance) </td>" + "<td> " + s[0] + "</td>")
                    self.write('</tr>')
                    
                        
                self.write("</table><b>GPA: "+ str(val.gpa) + "</b><br>")
                #results_list.append(val.gpa)
                #self.main_results_list.append(results_list)
                #self.main_results_list.append(results_list)
                #self.render("resultsdisp.html",results_list=self.main_results_list)
                #self.main_results_list = []
                
                #self.write("<b>GPA: " + val.gpa + "</b>")
            
            # if any('WH' in r for r in results):
            
            #elif len(r) == 12 and any(r[9:] in i for i in inv_list):
                #self.write("<br> Numbers present in Invalid Numbers List<br>")
            #   pass
            
            # elif len(r) == 11 and any(r[8:] in i for i in inv_list):
                #self.write("<br>Numbers present in Invalid List <br>")
            #    pass
                
                
            else:
                if len(r) == 12:
                    collcode = reg[0:4]
                    yearofjoin = reg[4:6]
                    dept = reg[6:9]
                    rollno = reg[9:]
                    info = (collcode,yearofjoin,dept,rollno)
                    
                elif len(r) == 11:
                    collcode = reg[0:3]
                    yearofjoin = reg[3:5]
                    dept = reg[5:8]
                    rollno = reg[8:]
                    info = (collcode,yearofjoin,dept,rollno)
                #res,k_v  = self.fetch_from_au(r)
                
                #url = "http://schools9.com/anna-university-grade-system-chennai.aspx?htno="+r
                #url = "http://www.annauniv.edu/cgi-bin/result/cgrade.pl?regno="+r
                #url = "http://schools9.com/anna-university-grade-system-exam-results200114.aspx?htno="+r
                url = "http://result.annauniv.edu/cgi-bin/divum/grad1414g1.pl?regno="+r
                rpc = urlfetch.create_rpc(deadline=60)
                urlfetch.make_fetch_call(rpc, url)
                
                rpc.callback = self.create_callback(rpc,r,info)
                rpcs.append(rpc)               
                
                
                
                
                #res,k_v = self.fetch_from_s9(r)
                
                
                """
                
                if res is not None:
                #self.write(results)
                    self.process_result(r,res,info,k_v)
                    #r_entity_list.append(r_entity)
                    
                else:
                    #self.write("<br>No Results for "+r)
                    if len(r) == 12:
                        invalidnumbers = invalidnumbers + r[9:] + ' '
                    elif len(r) == 11:
                        invalidnumbers = invalidnumbers + r[8:] + ' '                
        
        
        #if r_entity_list:
        #    db.put(r_entity_list)
        
        
        
        if  inv is not None and invalidnumbers:
        
        
            inv_num = models.InvalidNum(key_name = reg)
            inv_num.missingnumbers = inv.missingnumbers + invalidnumbers
            
            inv_num.put()
        
        elif inv is None and invalidnumbers:
            inv_num = models.InvalidNum(key_name = reg)
            inv_num.missingnumbers = invalidnumbers
            
            inv_num.put()
        
                """
        
        for rpc in rpcs:
            rpc.wait()
        
        #self.write(self.invalidnumbers)
        
        if  inv is not None and self.invalidnumbers:
        
        
            inv_num = models.InvalidNum(key_name = reg+"_"+self.key_val)
            inv_num.missingnumbers = inv.missingnumbers + self.invalidnumbers
            
            inv_num.put()
            
            #memcache.set(reg+"invalidnumbers",inv_num.missingnumbers)
        
        elif inv is None and self.invalidnumbers:
            inv_num = models.InvalidNum(key_name = reg+"_"+self.key_val)
            inv_num.missingnumbers = self.invalidnumbers
            inv_num.put()
            
            # memcache.set(reg+"invalidnumbers",inv_num.missingnumbers)
        
        #self.write(self.main_results_list)
        #self.render("resultsdisp.html",results_list=self.main_results_list)
        
        dr = models.DeptResult(key_name=reg+"_"+self.key_val,status="OK")
        
        dr.dept = reg
        
        dr.put()
        
        memcache.set(reg+"_"+self.key_val+"dept","OK")
        
        
   
   



class InvalidNumHandler(ResultHandler):
    
    def get(self):
        
        self.write('<form method="post" action="/dept/InvalidNum">')
        self.write('<input type="text" name="keyval"> <br> ')
        self.write('<textarea name="invalidnum"></textarea>')
        self.write('<input type="submit">')
    
    def post(self):
        
        keyval = self.request.get('keyval')
        iv = self.request.get('invalidnum')
        
        r = models.InvalidNum(key_name=keyval, missingnumbers=iv)
        r.put()
        
    
        
class DeptEViewHandler(ResultHandler):
    #Handler for Excel Sheet Display.
    #Separate Handler is used to create it separately and test.
    #This will the main department view Handler once it is ready
    
    
    def getsummary(self,grade_count):
        
        num_pass = grade_count['S']+ grade_count['A']+grade_count['B']+grade_count['C']+ grade_count['D']+grade_count['E'] 
        num_arrear = grade_count['U'] 
        num_absent = grade_count['UA']
        num_WH = grade_count['WH1'] + grade_count['WH2'] + grade_count['WH3'] + grade_count['WH4'] + grade_count['WH5'] + grade_count['WH6'] + grade_count['WH7'] + grade_count['WH8'] + grade_count['WH9'] + grade_count['WH10'] + grade_count['WH11'] +  grade_count['WH12']
        num_SA = grade_count['SA'] 
        summ_list = []
        if not any([num_arrear,num_WH,num_absent,num_SA]):
            return 'ALL PASS'
        
        else:
            if num_pass:
                summ_list.append('PASS in ' + str(num_pass))
            
            if num_arrear:
                summ_list.append('U:' + str(num_arrear) )
            
            if num_absent:
                summ_list.append('UA: ' + str(num_absent))
            
            if num_WH:
                summ_list.append('WH: ' + str(num_WH))
            
            if num_SA:
                summ_list.append('SA: ' +  str(num_SA))
        
        
        return ','.join(summ_list)
        
        
                
        
        
        
        
    
    
    
    def findsem(self,sem_key):
        
        year = sem_key[0]
        sem_type  = sem_key[2:]
        
        # Example of sem_key : 2_2 
        # In 2_2, the first 2 refers to the Year of Study. The other 2 refers to the semester of that year.
        # So, 2_2 means 2nd year and 2nd sem in that year => 4th Semester Results for the Department.
        
        if sem_type == '1':
            sem = ( int(year) * 2 ) - 1
            
        elif sem_type == '2':
            sem = ( int(year) * 2 )
        
        return sem
    
    

    
    
    
    
    def get(self,reg):
        
        #self.render("depte.html")
        
        
        
        deptstatus = models.DeptResult.all(keys_only=True).filter('dept = ',reg).order('-__key__').fetch(1)
        
    
        if deptstatus:
            
            #If deptstatus exists, then there is a Department Result associated with the given reg parameter in the URL.
            #So, datastore fetch is required.
             
            for each in deptstatus:
                deptres = db.get(each)
                dept_key = deptres.key().name() #TODO: Put in memcache.
                #self.write(dept.status)
                
                #To identify the key value appended to the reg parameter in the URL. eg) 2_2
                if len(reg) == 9:
                    sem_key = dept_key[10:]
                    dept = reg[6:]
                
                elif len(reg) == 8:
                    sem_key = dept_key[9:]
                    dept = reg[5:]
                    
                self.write(sem_key)
                sem = self.findsem(sem_key)
                #self.write(sem)
                #self.write(dept)
                dkey = db.Key.from_path('Department',dept)
                #self.write(dkey)
                sd = models.SubDept.all().filter('dept = ',dkey).filter('sem = ',str(sem))
                
                
                curr_sem_sub = [ (s.sub.key().name(),s.sub.subname) for s in sd ]
                
                subcodes = [ s.sub.key().name() for s in sd ]
                
                self.write('<div class="fix">')     
                for (s,sname) in curr_sem_sub:
                    
                    self.write( s +  "&nbsp;" +  sname )
                    self.write('<br>')
                    
                self.write('</div>')
             
                
                
            if deptres.status == 'OK':
                
                #Retrieving from memcache
                dr = deserialize_entities(memcache.get('dr_'+reg))
                
                if not dr:
                    #If dr is None, then we are fetching from datastore and putting it into memcache.
                    
                    dr = models.Result.all().filter('cyd_sem = ',reg+"_"+sem_key)
                    self.write(reg)
                    memcache.set('dr_'+reg, serialize_entities(dr))
                    self.write("Memcaching now only")
                    
                others = []
                current = []
                curr_sem_dict = {}
                
                
                #self.write('<table border=1>')
                #self.write('<tr class="f1">')
                #self.write('<th>Register Number </th><th> Name </th>')
                #for (s,sname) in curr_sem_sub:
                #    self.write('<th>'+ s + '</th>')
                
                #self.write('<th>Other Results</th>')
                #self.write('</tr>')
                
              
                
                for each in dr:
                    #self.write('<tr>')
                    
                    #self.write("<br>"+each.name + " " + each.gpa + "<br>")
                    #self.write('<td>' + each.regno + '</td>')
                    #self.write('<td>' + each.name + '</td>')
                    self.write(each.cyd_sem)
                    grade_count = Counter()
                    prop = each.dynamic_properties()
                    res_content = ''
                    
                    
                    
                    
                    
                    
                    for s in sorted(prop):
                            
                        if s in subcodes:
                            sub = getattr(each,s)
                            
                            sub = sub.split('  ')
                            current.append( (s,sub[2]) )
                            
                            grade_count[sub[2]] += 1 
                                
                            
                            #self.write(s)
                            #self.write(sub + "<br>")
                            #self.write('<td>' + sub[2] + '</td>')
                            
                            
                        else:
                            
                            #sub = sub.split('  ')
                            #grade = sub[2]
                            #subname = sub[3]
                            #res_content[3].replace(' ',"&nbsp;")
                            #self.write(res_content)
                            #html_content = cgi.escape(str(sub),quote=True)
                            #self.write(sub)
                            
                            #sub = [ str(s) for s in sub ]
                            #sub[3].replace(' ',"&nbsp;")
                            #self.write(sub)
                            #self.write("<br>")
                            
                            
                            sub = getattr(each,s)
                            others.append(sub)
                            
                            grade_count[sub.split('  ')[2]] += 1 
                            
                            
                            
                            
                    #self.write('')        
                    others = ''.join(others)
                    
                    
                    res_summary = self.getsummary(grade_count)
                    
                    #self.write(each.regno + res_summary + "<br>")
                    
                
                    curr_sem_dict[(each.regno)] = (each.name,current,each.gpa,others,res_summary)
                    
                    current = []
                    others = []
                    #self.write(each.regno +  str(grade_count) )
                    #grade_count = Counter()
                    
                
                    #self.write('<td> <button onclick = openWin2(%s) >  Show Others </button>' %(sub))
                    #self.write('</td></tr>')
                #self.write('</table>')        
            #self.write(curr_sem_dict)  
            self.render("depte.html",curr_sem_sub = curr_sem_sub,subcodes = subcodes,func=foo,var_res='',curr_sem_dict= curr_sem_dict)
            
            #memcache.set('/depte/%s' % (reg),html)
                
                
                
        
        
        
        
        
        
        


    
    
    
            
app = webapp2.WSGIApplication([('/', MainPage),
                               #('/mu-d224a26f-80ad006a-942e1af7-295961b6',Blitz),
                               ('/dept/([0-9]+)',DeptViewHandler),
                               ('/dept/InvalidNum',InvalidNumHandler),
                               ('/depte/([0-9]+)',DeptEViewHandler)
                              
                               
                               ],
                              debug=True)
def main():
    app.run()
    

if __name__=='__main__':
    main()